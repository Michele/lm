using Optim

# Data
# MGH09 data: http://www.itl.nist.gov/div898/strd/nls/data/mgh09.shtml

x=[4.000000E+00,
    2.000000E+00,
    1.000000E+00,
    5.000000E-01,
    2.500000E-01,
    1.670000E-01,
    1.250000E-01,
    1.000000E-01,
    8.330000E-02,
    7.140000E-02,
    6.250000E-02]

y=[1.957000E-01,
    1.947000E-01,
    1.735000E-01,
    1.600000E-01,
    8.440000E-02,
    6.270000E-02,
    4.560000E-02,
    3.420000E-02,
    3.230000E-02,
    2.350000E-02,
    2.460000E-02]

function m(x,b::Array{Float64,1})
	den=x.*x+b[3]*x+b[4]
    fact=x.*(x+b[2])
    return (b[1]*fact)./den
end

function f(b::Array{Float64,1})
	den=x.*x+b[3].*x+b[4]
    fact=x.*(x+b[2])
    return y - (b[1].*fact)./den
end

function g(b::Array{Float64,1})
	den=x.*x+b[3].*x+b[4]
    fact=x.*(x+b[2])
	dydb=zeros((length(x),4))
    dydb[:,1] = fact./den
    dydb[:,2] = b[1]*x./den
    dydb[:,4] = -b[1]*fact./(den.*den)
    dydb[:,3] = dydb[:,4].*x
    # println(dydb)
    return -dydb
end
# x0=[1.9280693458E-01, 1.9128232873E-01, 1.2305650693E-01, 1.3606233068E-01]
x0=[25, 39, 41.5, 39]/10
Optim.levenberg_marquardt(f,g,x0,maxIter=500,show_trace=true)