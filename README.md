## A CUDA kernel implementing the Levenberg-Marquardt algorithm.

Finds argmin(x) sum(f(x)^2) using the Levenberg-Marquardt algorithm
It accepts as input two pointers to `__device__` functions:

- f: R^n -> R^m
- g: R^n -> R^(m*n)

Function f should take an input vector of length n and return an output vector of length m.
Function g is the Jacobian J of f, and should be an m x n matrix

The function prototype of f and g is `f(REAL x[], REAL *y, const int n, const int m )`. In place of `*y` The output of g should be `*J`.

The output is in the array `x0[]`, whereas the number of iteration actually used for computing the minimum is pointed by `*maxiter`.

System (J^T *J) * h + diag(sqrt(lambda)*J) = -J^T * y is solved using Cholesky factorization.

Step quality strategy has been used to accept the step of the algorithm.


The structure and the output format is inspired by the implementation of the LMA in julia (Optim.jl)
