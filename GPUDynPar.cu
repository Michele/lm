/** 
 * @file GpuLogLike.cxx
 * @brief LogLike class implementation with GPU
 * @author 
 *
 */

#include "Likelihood/GpuLogLike.h"
#include "Likelihood/GpuSpectra.cu"
#include "Likelihood/FileFunction.h"

namespace Likelihood {

// KERNEL /////////////////////////////////////////////////////////////////////////////////////////////

__global__ void fluxDensity(int * numbers, double * params, double * energies, double * ref, double * results) {
  int i = threadIdx.x + blockIdx.x * blockDim.x,
      n_events = numbers[0],
      n_sources = numbers[1],
      excluded_source = numbers[2];
  if (i < n_events) {
    results[i] = 0;
    for(int j=0; j<n_sources; j++) {
      if(j != excluded_source)
        results[i] += params[2*j]*pow(energies[i], params[2*j+1]) * ref[j*n_events+i];
    }
    results[i] = results[i]>0. ? log(results[i]) : 0;
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////

GpuLogLike::GpuLogLike(const Observation & observation) : 
  LogLike(observation),
  init(0)
  {
    std::cout << ":: GPU Likelihood ::" << std::endl;
  }

__global__ double theValue(int *numbers, double *params, double *energies, double *ref) {

  // call fluxDensity kernel, passing pointers to number of events and sources, spectra, parameters, energies, ref and results
  fluxDensity<<<(numbers[0]/256)+1,i 256>>>(numbers, params, energies, ref, results);

  // reduce results to sum (to be moved to device)
  double my_value = 0.0;
  for (int i=0; i<numbers[0]; i++) my_value += host_results[i];
  
  return my_value;
}


} // namespace Likelihood
