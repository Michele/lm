#include <stdio.h>
#include <stdlib.h>
#include <algorithm> // for fill_n
#include "log_likelihood_lm_gpu.h"

#define VARPERSRC 2

__device__ double GpuPowerLawValue(double energy, double *parameters) {
  // ordered parameters are Prefactor, Index, Scale
  return parameters[0] * pow(energy/parameters[2], parameters[1]);
}

__device__ double (*spectra)(double, double *) = &GpuPowerLawValue;

__global__ void fluxDensity(const int *numbers, const double *params, const double *energies, const double *ref,
                            double *results)
{
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  const int n_events = numbers[0];
  const int n_sources = numbers[1];
  const int excluded_source = numbers[2];
  if (i < n_events) {
    results[i] = 0.0;
    for(int j=0; j<n_sources; j++) {
      if(j != excluded_source)
        results[i] += params[2*j]*pow(energies[i], params[2*j+1]) * ref[j*n_events+i];
    }
    results[i] = results[i]>0.0 ? log(results[i]) : 0.0;
  }
}

//__device__ double my_value;
// TODO, maybe theValue is not needed
__global__ void theValue(const int *numbers, const double *params, const double *energies, const double *ref,
                double *results)
{

  // call fluxDensity kernel, passing pointers to number of events and sources, spectra, parameters, energies, ref and results
  fluxDensity<<<(numbers[0]/256)+1, 256>>>(numbers, params, energies, ref, results);

  // reduce results to sum //FIXME decide if it is to be done in the outer kernel
//  my_value = 0.0;
//  for (int i=0; i<numbers[0]; i++)
//    my_value += results[i];
}

__global__ void grad_fluxDensity(const int *numbers, const double *params, double const *energies, double const *ref, //inputs
                                  double *denom, // auxiliary //TODO maybe this can be put in the device
                                  double *grad) // output
{
  int i = threadIdx.x + blockIdx.x * blockDim.x; // corresponds to the event
  int k = threadIdx.y + blockIdx.y * blockDim.y; //FIXME varies from 0 to VARPERSRC*n_sources
  const int n_events = numbers[0];
  const int n_sources = numbers[1];
  const int v = VARPERSRC * n_sources; // total number of variables
  const int excluded_source = numbers[2];
  if (i < n_events) {
      denom[i]=0;
    grad[k*v+i]=0.0;
    //for (int k=0;k<v;k++){
    if (k % 2) { // index is odd: computing derivative w.r.t. b_k
      if((k-1)>>1 != excluded_source) {
        grad[k*v+i] = -params[k-1]*pow(energies[i], -params[k]) * log(energies[i]) * ref[((k-1)>>1)*n_events+i];
      for(int j=0; j<n_sources; j++) { // k=2*j+1 -> j=(k-1)/2
          denom[i] += params[2*j]*pow(energies[i], -params[2*j+1]) * ref[j*n_events+i];
        }
      }
      grad[k*v+i] /= denom[i];
    } else { // index is even: computing derivative w.r.t. A_k
      if(k>>1 != excluded_source) {
        grad[k*v+i] = pow(energies[i], -params[k+1]) * ref[(k>>1)*n_events+i];
      for(int j=0; j<n_sources; j++) { // k=2*j -> j=k/2
          denom[i] += params[2*j]*pow(energies[i], -params[2*j+1]) * ref[j*n_events+i];
        }
      }
      grad[k*v+i] /= denom[i];
    }
  }
}

__global__ void theGrad(const int *numbers, const double *params, const double *energies, const double *ref,
                                double *denom, //auxiliary memory useful for the denominator of the gradient
                                double *grad,
                                double *my_grad)
{
  uint numThreads=256;
  int n_events = numbers[0];
  int n_sources = numbers[1];
  int v = VARPERSRC * n_sources; // number of free variables
  dim3 numBlocks((n_events/numThreads)+1, v);
  // call fluxDensity kernel, passing pointers to number of events and sources, spectra, parameters, energies, ref and results
  grad_fluxDensity<<<numBlocks, numThreads>>>(numbers, params, energies, ref, denom, grad); //FIXME rework numblocks and numThreads
  // wait until all the threads have finished before reduction
  cudaDeviceSynchronize();
  // reduce results to the real value of the gradient
  for (int k=0; k<v; k++)
  {
      my_grad[k]=0;
      for (int i=0; i<n_events; i++)
          my_grad[k] += grad[k*v+i];
  }
}
//TODO
/*
__global__ void fluxDensity_deriv(const int *numbers, const double *params, double const *energies, double const *ref, //inputs
                                  double *deriv) // output dimensions (VARPERSRC*n_sources)^2
{
  int i = threadIdx.x + blockIdx.x * blockDim.x; // corresponds to the event
  int p = threadIdx.y + blockIdx.y * blockDim.y; // varies from 0 to VARPERSRC*n_sources
  int q = threadIdx.z + blockIdx.z * blockDim.z; // varies from 0 to VARPERSRC*n_sources

  // double deriv_p, deriv_q;

  const int n_events = numbers[0];
  const int n_sources = numbers[1];
  const int v = VARPERSRC * n_sources; // total number of variables
  const int excluded_source = numbers[2];
  if (i < n_events) {
    deriv[p+v*q]=0.0;
    if(p != excluded_source)
            results[i] = params[2*j]*pow(energies[i], params[2*j+1]) * ref[j*n_events+i];
    //for (int p=0;p<v;p++){ //TODO
    if (p % 2) { // index is odd: computing derivative w.r.t. b_k
      if((p-1)>>1 != excluded_source) {
        deriv_p = -params[p-1]*pow(energies[i], -params[p]) * log(energies[i]) * ref[((p-1)>>1)*n_events+i];
      for(int j=0; j<n_sources; ++j) { // p=2*j+1 -> j=(p-1)/2
          denom[i] += params[2*j]*pow(energies[i], -params[2*j+1]) * ref[j*n_events+i];
        }
      }
      deriv[p*v+q] /= denom[i];
    } else { // index is even: computing derivative w.r.t. A_k
      if(p>>1 != excluded_source) {
        deriv[p*v+q] = pow(energies[i], -params[p+1]) * ref[(p>>1)*n_events+i];
      for(int j=0; j<n_sources; j++) { // p=2*j -> j=p/2
          denom[i] += params[2*j]*pow(energies[i], -params[2*j+1]) * ref[j*n_events+i];
        }
      }
      deriv[k*v+i] /= denom[i];
    }
  }
}
*/

__global__ void levenberg_marquardt(
    /*func_t func, func_t grad,*/ /*const int n_events, const int n_sources,*/
    REAL x0[], const int n,/* const int m,*/
    REAL *y, /*REAL *J,*/ REAL *H, REAL *A, REAL *g,  REAL d[],
    REAL *h, REAL *trial_x, REAL *trial_y, int *maxiter,
    const int *numbers, /*double *params,*/ double *energies, double* ref, double * grad, double *results)
/*
finds argmin(x) sum(-log(f(x))) using the Levenberg-Marquardt algorithm
f = sum_{n_s}_j {flux_density_j(E_i,{\alpha})}

- n is the number of free parameters
- y is the array collecting the f_i values and it is of length n_events = numbers[0]
- x0 is an initial guess for the solution
*/
{
    // int NUMBLOCK = n_events/512+1;
    // int NUMTHREAD = 512;
    int i, j, k;
    //int var_par;
    int converged=0;
    int iter_counter=0;
    REAL MIN_STEP_QUALITY= 1e-3;
    REAL TOL_GRAD=1e-12;
    // int DEBUG=0;
    // int VERBOSE=0;
    //REAL TOL_X=1e-5;

    REAL *x = x0;
    REAL log_like=0.0; // this is sum(ln(f(x)))
    REAL log_like_trial=0.0;
    REAL lambda=100.0;
    REAL rho=0.0;
    REAL rho_denom=0.0;
    // REAL g_upd=0.0; // FIXME now is a vector
    REAL norm_inf=0.0;
    REAL sum=0.0; // buffer for summations
    
    const int n_events = numbers[0];
    // const int n_sources = numbers[1];
    // for (var_par=0,j=0;j<n;j++)
    //     if (!fixed_x[j]) var_par++;

    while (!converged && iter_counter<*maxiter)
    {
    ++iter_counter;
    // Compute f and residuals
    //(*func)(x,y,n,m);

    theValue<<< 1,1 >>>(numbers, x, energies, ref, y);
    cudaDeviceSynchronize();
    for (log_like=0.0,i=0;i<n_events;i++)
        log_like += y[i];

    // print y
    if(DEBUG){
    printf("\nNEWSTEP\n");
    for (i=0;i<n_events;i++)
        printf("y[%i]=%g\n",i,y[i]);
    }
    
    if(DEBUG)
        printf("log_like=%g\n", log_like);

    theGrad<<< 1,1 >>>(numbers, x, energies, ref, results, grad, g);
    cudaDeviceSynchronize();

    // print g
    if(DEBUG){
    for (i=0;i<n;i++) // n is the number of free parameters
        printf("g[%i]=%g\n",i,g[i]);
    }

    //compute the Hessian approximation as per MINUIT tutorial §5.2
    fluxDensity_deriv<<<,,>>>(numbers, x, energies, ref, H);
    cudaDeviceSynchronize();
    for (int p=0;p<n;++p)
        for (int q=0;q<n;++q)
        {
            for (int i=0;i<n_events;i++)
                H[p+n*q]+=1/(y[i]*y[i]) ;
        }
//    // print J
//    if(DEBUG){
//      for (i=0;i<m;i++)
//          for (j=0;j<n;j++)
//              printf("*(J+%i*%i+%i)=%g\n",i,n,j, J[i*n+j]);
//    }


//    // print H
//    if(DEBUG){
//    printf("----- H ------\n");
//    for (i=0;i<n;i++)
//        for (j=0;j<n;j++)
//            printf("H[%i*%i+%i]=%g\n",i,n,j, H[i*n+j]);
//    }

//    // Compute g=-J^T*y
//    for (j=0;j<n;j++)
//    {
//        for (g[j]=0.0,i=0;i<m;i++)
//            g[j] += J[i*n+j] * y[i];
//        g[j] = -g[j]; // Be aware of the minus sign here.
//    }



    // Alter linearized fitting matrix, by augmenting diagonal elements.
    // Compute A = H + lambda*diag(H)
    for (i=0;i<n;i++)
    {
        for (j=0;j<n;j++)
            A[i*n+j] = H[i*n+j];
        A[i*n+i] += sqrtf(lambda) * H[i*n+i]; // note: added sqrt
    }

    // print A
    if(DEBUG){
    printf("----- A ------\n");
    for (i=0;i<n;i++)
        for (j=0;j<n;j++)
            printf("*(A+%i*%i+%i)=%g\n",i,n,j, *(A+i*n+j));
    }

    // Solve the linear system A*h=g Using Cholesky decomposition
    // Compute Cholesky factorization
    // TODO add the minus sign for solving Ah=-g
    for (i=0;i<n;i++)
    {
        for (j=i;j<n;j++)
        {
            for (sum=A[i*n+j],k=i-1;k>=0;k--)
            {
                // printf("k=%i\n",k);
                sum -= A[i*n+k] * A[j*n+k];
                // printf("sum=%g\n", sum);
            }
            if (i == j)
            {
                if (sum <= 0.0) // A, with rounding errors, is not positive definite.
                {
                    printf("cholesky failed\n");
                    return;
                }
                // printf("i=%i\n", i);
                d[i]=sqrtf(sum);
                // printf("d[%i]=%g\n",i,d[i] );
            }
            else A[j*n+i]=sum/d[i];
        }
    }

    // print cholesky(A)
    if(DEBUG){
    printf("\nCholesky of A\n");
    for (i=0;i<n;i++)
        for (j=0;j<=i;j++) //print only lower triangular
        {
            if (i==j)
                printf("A[%i*%i+%i]=%g\n",i,n,j, d[i]);
            else
                printf("A[%i*%i+%i]=%g\n",i,n,j, A[i*n+j]);
        }
    }
    // Solve the systems
    for (i=0;i<n;i++)// Solve L*y = b, storing y in x.
    {
        for (sum=g[i],k=i-1;k>=0;k--) sum -= A[i*n+k] * h[k]; //FIXME add the minus sign here to g
        h[i]=sum/d[i];
    }

    for (i=n-1;i>=0;i--) //Solve L^T*x = y.
    {
        for (sum=h[i],k=i+1;k<n;k++) sum -= A[k*n+i] * h[k];
        h[i]=sum/d[i];
    }

    // print the solution of the system, h
    if(DEBUG){
    for (i=0;i<n;i++)
        printf("h[%i]=%g\n",i,h[i] );
    }

    // Compute the linearized model difference (L(0)-L(h))
    rho_denom = 0;
    for (i=0;i<n;++i)
    {
        rho_denom += H[i+n*i]*h[i]*h[i]*(0.5+lambda);
        for (j=0;j<i;++j)
        {
            rho_denom += H[i+n*j]*h[i]*h[j];
        }
    }

    // check the residuals
    // now trial_y is used as a buffer for computing the predicted_residual
    // trial_y=J*h+y
    // for (i=0;i<m;i++)
    // {
    //     for (trial_y[i]=0.0,j=0;j<n;j++)
    //         trial_y[i] += J[i*n+j] * h[j];
    //     trial_y[i] += y[i];
    // }
    // for (predicted_residual=0.0,i=0;i<m;i++)
    //     predicted_residual += trial_y[i];

    for (j=0;j<n;j++)
        trial_x[j] = x[j] + h[j];

    theValue<<< 1,1 >>>(numbers, trial_x, energies, ref, trial_y);
    cudaDeviceSynchronize();

    for (log_like_trial=0.0,j=0;j<n_events;j++)
        log_like_trial += trial_y[j];

    // step quality = residual change / predicted residual change
    // rho = (F(x) - F(x+h)) / (L(0) - L(h))
    rho = (log_like - log_like_trial) / rho_denom;
    
    // print rho
    if(DEBUG){printf("rho=%g\n",rho);}

    if (rho > MIN_STEP_QUALITY)
    {
        for (j=0;j<n;j++)
            x[j] += h[j];
            
        // for (i=0;i<m;i++)
        //     y[i]=trial_y[i];
        
        log_like=log_like_trial;
        lambda *= 0.1;
    }
    else lambda *= 10.0;

    // Compute the infinity norm of g_upd=J^T*y
    // theGrad<<< 1,1 >>>(numbers, x, energies, ref, results, grad, g);
    cudaDeviceSynchronize();
    norm_inf=0.0;
    theGrad<<< 1,1 >>>(numbers, x, energies, ref, results, grad, trial_y); // trial_y used as a buffer
    cudaDeviceSynchronize();
    sum=0;
    for (j=0;j<n;j++)
    {
        // for (g_upd=0.0,i=0;i<m;i++)
        //     g_upd += J[i*n+j] * y[i];
        sum += trial_y[j];
        if(DEBUG){printf("g_upd=%g\n",trial_y);}

        if (sum < 0) sum = -sum; // take the abs.
        if (sum > norm_inf)
            norm_inf = sum;
    }
    if (norm_inf < TOL_GRAD) converged=1;
    //else if //TODO (michele)

//=====================
    if (VERBOSE || DEBUG){
        printf("\n==== iter %3i ====\n",iter_counter);
        printf("norm_inf=%g\n",norm_inf);
        printf("lambda=%g\n",lambda);
        for (i=0;i<n;i++)
            printf("h[%i]=%g\n",i,h[i] );
    }
    } //while loop
    *maxiter=iter_counter;
} // levenberg_marquardt

// Pointers to the chosen device functions
// FIXME (michele)
// __device__ func_t p_func = FUNC;
// __device__ func_t p_grad = GRAD;

int main(int argc, char const *argv[])
{
    // const int n_events=20000;
    const int n_events=10;
    // const int n_sources=50;
    const int n_sources=2;
    const int n_params=VARPERSRC;
    const int n=n_params*n_sources; // number of free variables
    // const int m=n_sources;   //TODO remove me  // number of dimension of the codominion of f. f is scalar! m=1

    int MAXITER=100;
    int *p_maxiter = &MAXITER;
    int i,j;

    // input
    double params[] = {100,5,3,2}; // of length n_sources * VARPERSRC = n
    double energies[] = {2e5,10e5,8e5,3.4e5,76e5,0.2e5,5e5,2.3e5,1e5,1e7}; // of length n_events

    double *ref = new double[n_sources*n_events];
    // initialized to 1 (neutral element for a multiplicative factor) since for now we don't have any usable value.
    std::fill_n(ref, n_sources*n_events, 1);

    double *p_params     = params,
           *tmp_energies = energies;

    // Results
    REAL minimizer[n];
    REAL minimum[m_events];
    REAL scalar_res;
    int iter=0;

    cudaError_t err;
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    // Define functions pointer
    // func_t myfunc;
    // func_t mygrad;

    // Load into the above defined pointers the device functions to be used
    // HANDLE_ERR( cudaMemcpyFromSymbol( &myfunc, p_func, sizeof( func_t ) ));
    // HANDLE_ERR( cudaMemcpyFromSymbol( &mygrad, p_grad, sizeof( func_t ) ));

    size_t sizeN  = n*sizeof(REAL);
    // size_t sizeM  = m*sizeof(REAL); // TODO remove me
    size_t sizeNN = n*n*sizeof(REAL);
    // size_t sizeMN = m*n*sizeof(REAL); // TODO remove me
    //size_t isizeN = n*sizeof(int);
    // TODO change n,m nel numero di sorgenti e fotoni
    // Pointers to data which will be allocated to the device
    REAL  *d_y      =0,
//          *d_J      =0,
          *d_H      =0,
          *d_A      =0,
          *d_g      =0,
          *d_diag   =0,
          *d_h      =0,
          *d_trial_x=0,
          *d_trial_y=0;

    int  *dev_numbers =0;
            
    double  *dev_energies=0,
            //*dev_spectra =0,
            // *dev_params  =0,
            *dev_results = 0,
            *dev_grad = 0,
            *dev_ref = 0;

    REAL *d_x0=0;
    
    int  *d_maxiter=0;

    // Allocate vectors and matrices
    
    HANDLE_ERR( cudaMalloc((void**)&d_x0,sizeN) );
    HANDLE_ERR( cudaMemcpy(d_x0,p_params,sizeN,cudaMemcpyHostToDevice) );
    
    HANDLE_ERR( cudaMalloc((void**)&d_y,n_events*sizeof(REAL)) );
    HANDLE_ERR( cudaMalloc((void**)&d_trial_y,n_events*sizeof(REAL)) );
    
//    HANDLE_ERR( cudaMalloc((void**)&d_J, sizeMN) );

    HANDLE_ERR( cudaMalloc((void**)&d_H, sizeNN) );

    HANDLE_ERR( cudaMalloc((void**)&d_A, sizeNN) );

    HANDLE_ERR( cudaMalloc((void**)&d_g,sizeN) );

    HANDLE_ERR( cudaMalloc((void**)&d_diag,sizeN) );

    HANDLE_ERR( cudaMalloc((void**)&d_h,sizeN) );

    HANDLE_ERR( cudaMalloc((void**)&d_trial_x,sizeN) );

    HANDLE_ERR( cudaMalloc((void**)&d_maxiter,sizeof(int)) );
    HANDLE_ERR( cudaMemcpy(d_maxiter,p_maxiter,sizeof(int),cudaMemcpyHostToDevice) );


    ////////////////// For computing fluxDensity
    int tmp_numbers[3];
    tmp_numbers[0] = n_events;
    tmp_numbers[1] = n_sources;
    tmp_numbers[2] = -1; //excluded source (-1 means no excluded source)

    // allocating dev arrays on the device
    HANDLE_ERR( cudaMalloc((void **)&dev_numbers, sizeof(int)*3) );

    // double(*spectra[n_sources])(double, double *);
    // cudaMalloc((void **)&dev_spectra, sizeof(double(*)(double, double *))*n_sources);
    //HANDLE_ERR( cudaMalloc((void **)&dev_spectra, sizeof(double(*)(double, double *))) );

    // HANDLE_ERR( cudaMalloc(&dev_params, sizeof(double *)*n_sources) );

    HANDLE_ERR( cudaMalloc((void **)&dev_energies, sizeof(double)*n_events) );

    HANDLE_ERR( cudaMalloc((void **)&dev_results, sizeof(double)*n_events) );

    HANDLE_ERR( cudaMalloc((void **)&dev_grad, sizeof(double)*n_events*VARPERSRC*n_sources) );

    HANDLE_ERR( cudaMalloc((void **)&dev_ref, sizeof(double)*n_events*n_sources) );

    // moving data to device, just once.
    HANDLE_ERR( cudaMemcpy(dev_numbers, tmp_numbers, sizeof(int)*3, cudaMemcpyHostToDevice) );

    //HANDLE_ERR( cudaMemcpy(dev_spectra, spectra, sizeof(spectra), cudaMemcpyHostToDevice) ); //FIXME

    // copy pointers to parameters array to device
    HANDLE_ERR( cudaMemcpy(dev_energies, tmp_energies, sizeof(double)*n_events, cudaMemcpyHostToDevice) );
    // HANDLE_ERR( cudaMemcpy(dev_params, p_params, sizeof(p_params), cudaMemcpyHostToDevice) );

    HANDLE_ERR( cudaMemcpy(dev_ref, ref, sizeof(double)*n_events*n_sources, cudaMemcpyHostToDevice) );

    // call fluxDensity kernel, passing pointers to number of events and sources, spectra, parameters, energies, ref and results
    //fluxDensity<<<(n_events/512)+1,512>>>(dev_numbers, dev_spectra, dev_params, dev_energies, dev_results); //FIXME

    //////////////////

    cudaEventRecord(start);
    levenberg_marquardt<<< 1,1>>>(/*myfunc,mygrad,*/
                                    d_x0, n, /*m,*/
                                    d_y, /*d_J,*/ d_H, d_A, d_g, d_diag,
                                    d_h, d_trial_x, d_trial_y, d_maxiter,
                                    dev_numbers, /*dev_spectra,*//* dev_params,*/ 
                                    dev_energies, dev_ref, dev_grad, dev_results);

    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaDeviceSynchronize(); // Guarantee kernel execution has finished
    err = cudaGetLastError();
    if (err != cudaSuccess) printf("error: %s\n",cudaGetErrorString(err));

    // Copy back the results
    HANDLE_ERR( cudaMemcpy(minimizer,d_x0,sizeN,cudaMemcpyDeviceToHost) );
    HANDLE_ERR( cudaMemcpy(minimum,d_y,n_events*sizeof(REAL),cudaMemcpyDeviceToHost) ); // 
    HANDLE_ERR( cudaMemcpy(&iter,d_maxiter,sizeof(int),cudaMemcpyDeviceToHost) );


    // Compute value of function at minimum
    for (scalar_res=0.0,j=0;j<n_events;j++) // TODO maybe remove this reduction and use the log_like of the kernel 

        scalar_res += minimum[j];

    // Print results
    printf("\n==== RESULTS ====\n");
    printf("Levenberg Marquardt algorithm:\n");
    printf(" * Starting point: \n\tx0 = [ ");
    for (i=0;i<n;i++) printf(" %g ", p_params[i]);
    printf(" ]\n");
    printf(" * Minimum: \n\tx  = [ ");
    for (i=0;i<n;i++) printf(" %g ", minimizer[i]);
    printf(" ]\n");
    printf(" * Value of Function at Minimum: \n\t%g\n",scalar_res);
    printf(" * Iterations: \n\t%i\n",iter);

    float milliseconds = 0.0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    printf("Kernel execution time: %g ms\n",milliseconds);

    //free memory
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    cudaFree(d_x0);
    cudaFree(d_y);
    cudaFree(d_trial_x);
    cudaFree(d_trial_y);
//    cudaFree(d_J);
    cudaFree(d_H);
    cudaFree(d_A);
    cudaFree(d_g);
    cudaFree(d_diag);
    cudaFree(d_h);
    cudaFree(d_maxiter);
    cudaDeviceReset();
    return err;
}
