/* Custom parameters */
#define REAL double

// #define DATASET 2_ex

// #define FUNC f_2ex
// #define GRAD g_2ex

#define FUNC fluxDensity
#define GRAD fluxDensityDerivs

#define DEBUG 0
#define VERBOSE 0

// Requires the definition of err
#define HANDLE_ERR(err) {												\
	if (err != cudaSuccess)	{											\
		fprintf(stderr, "CUDA error detected: %s in %s at line %d\n",	\
				cudaGetErrorString( err ), __FILE__, __LINE__);			\
	} }


typedef void (*func_t)(REAL[], REAL* , int , int );