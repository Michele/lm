#include <stdio.h>
#include <stdlib.h>
#include "lm_gpu.h"

/*
 MGH09 data: http://www.itl.nist.gov/div898/strd/nls/data/mgh09.shtml
*/
__constant__ __device__ REAL
x_data[11]={
    4.000000E+00,
    2.000000E+00,
    1.000000E+00,
    5.000000E-01,
    2.500000E-01,
    1.670000E-01,
    1.250000E-01,
    1.000000E-01,
    8.330000E-02,
    7.140000E-02,
    6.250000E-02};

__constant__ __device__ REAL
y_data[11]={
    1.957000E-01,
    1.947000E-01,
    1.735000E-01,
    1.600000E-01,
    8.440000E-02,
    6.270000E-02,
    4.560000E-02,
    3.420000E-02,
    3.230000E-02,
    2.350000E-02,
    2.460000E-02};

__device__ void vec_sqr(REAL vec[])
{
    int j=threadIdx.x;
    vec[j]*=vec[j];
}

/*__global__*/ __device__ void
f_2ex(REAL x[], REAL *y , const int n, const int m )
{   
    y[0]=x[0];
    y[1]=2.0 - x[1];
}

/*__global__ */ __device__ void
g_2ex(REAL x[], REAL *J, const int n, const int m )
{
    *J=1.0; // J[0][0]
    *(J+0*m+1)=0.0; // J[0][1]
    *(J+1*m+0)=0.0; // J[1][0]
    *(J+1*m+1)=-1.0; // J[1][1]
}

__device__ void
den_MGH09(REAL x[], REAL *y , const int n, const int m )
{   
    int i;
    for (i=0;i<m;i++)
        y[i]=x_data[i]*x_data[i]+x[2]*x_data[i]+x[3];
}

__device__ void
fact_MGH09(REAL x[], REAL *y , const int n, const int m )
{   
    int i;
    for (i=0;i<m;i++)
        y[i]=x_data[i]*(x_data[i]+x[1]);
}

/*__global__*/ __device__ void
f_MGH09(REAL x[], REAL *y , const int n, const int m )
{   
    int i;
    for (i=0;i<m;i++)
        y[i]=y_data[i]-(x[0]*x_data[i])*(x_data[i]+x[1])/(x_data[i]*x_data[i]+x[2]*x_data[i]+x[3]);
}

/*__global__ */ __device__ void
g_MGH09(REAL x[], REAL *J, const int n, const int m )
{
    int i;
    REAL *den=(REAL *)malloc(2*m*sizeof(REAL));
    REAL *fact=&den[m];
//    REAL den[m], fact[m];
    den_MGH09(x,den,n,m);
    fact_MGH09(x,fact,n,m);
    for (i=0;i<m;i++)
        *(J+i*n+0)=-fact[i]/den[i];

    for (i=0;i<m;i++)
        *(J+i*n+1)=-x[0]*x_data[i]/den[i];

    for (i=0;i<m;i++)
        *(J+i*n+3)=x[0]*fact[i]/(den[i]*den[i]);
    
    for (i=0;i<m;i++)
        *(J+i*n+2)=*(J+i*n+3)*x_data[i];
    free(den);
}

__global__ void levenberg_marquardt(
    func_t func, func_t grad,
    REAL x0[], const int n, const int m, int fixed_x[], int n_fix,
    REAL *y, REAL *J, REAL *JtJ, REAL *A, REAL *g,  REAL d[],
    REAL *h, REAL *trial_x, REAL *trial_y, int *maxiter)
/*
finds argmin(x) sum(f(x).^2) using the Levenberg-Marquardt algorithm
f: R^n -> R^m
g: R^n -> R^(m*n)

- The function f should take an input vector of length n and return an 
output vector of length m
- The function g is the Jacobian of f, and should be an m x n matrix
- x0 is an initial guess for the solution
*/
{
    //int NUMBLOCK = 1;
    //int NUMTHREAD = m;
    int i, j, k, var_par;
    int converged=0;
    int iter_counter=0;
    REAL MIN_STEP_QUALITY= 1e-3;
    REAL TOL_GRAD=1e-12;
    // int DEBUG=0;
    // int VERBOSE=0;
    //REAL TOL_X=1e-5;

    REAL *x = x0;
    REAL residual=0.0; // this is sum(f(x)^2)
    REAL predicted_residual=0.0;
    REAL trial_residual=0.0;
    REAL lambda=100.0;
    REAL rho=0.0;
    REAL g_upd=0.0;
    REAL norm_inf=0.0;
    REAL sum=0.0;
    
    for (var_par=0,j=0;j<n;j++)
        if (!fixed_x[j]) var_par++;

    while (!converged && iter_counter<*maxiter)
    {
    ++iter_counter;
    // Compute f and residuals
    //(*func)(x,y,n,m);
    (*func)(x,y,n,m);
    //(*func)<<< NUMBLOCK, NUMTHREAD >>>(x,y,n,m);

    // print y
    if(DEBUG){
    printf("\nNEWSTEP\n");
    for (i=0;i<m;i++)
        printf("y[%i]=%g\n",i,y[i]);
    }
    
    // vec_sqr<<< NUMBLOCK, NUMTHREAD >>>(y);

    for (residual=0.0,i=0;i<m;i++)
        residual += y[i]*y[i];

    if(DEBUG){printf("residual=%g\n", residual);}
    //(*grad)(x,J,n,m);

    (*grad)(x,J,n,m);

    // print J
    if(DEBUG){
    for (i=0;i<m;i++)
      for (j=0;j<n;j++)
        printf("*(J+%i*%i+%i)=%g\n",i,n,j, J[i*n+j]);
    }

    // create matrices

    // Compute JtJ
    for (i=0;i<n;i++)
        for (j=0;j<n;j++)
        {
            for (sum=0.0,k=0;k<m;k++)
                sum += J[k*n+i]*J[k*n+j];
            JtJ[i*n+j] = sum;
        }

    // print JtJ
    if(DEBUG){
    printf("-----JtJ------\n");
    for (i=0;i<n;i++)
        for (j=0;j<n;j++)
            printf("JtJ[%i*%i+%i]=%g\n",i,n,j, JtJ[i*n+j]);
    }

    // Alter linearized fitting matrix, by augmenting diagonal elements.
    // Compute A=JtJ+lambda*diag(JtJ)
    for (i=0;i<n;i++)
    {
        for (j=0;j<n;j++)
            A[i*n+j] = JtJ[i*n+j];
        A[i*n+i] += sqrtf(lambda) * JtJ[i*n+i]; // note: added sqrt
    }

    // print A
    if(DEBUG){
    printf("----- A ------\n");
    for (i=0;i<n;i++)
        for (j=0;j<n;j++)
            printf("*(A+%i*%i+%i)=%g\n",i,n,j, *(A+i*n+j));
    }

    // Compute g=-J^T*y
    for (j=0;j<n;j++)
    {
        for (g[j]=0.0,i=0;i<m;i++)
            g[j] += J[i*n+j] * y[i];    
        g[j] = -g[j]; // Be aware of the minus sign here.
    }

    // print g
    if(DEBUG){
    for (i=0;i<n;i++)
        printf("g[%i]=%g\n",i,g[i]);
    }
    // Solve the linear system A*h=g Using Cholesky decomposition
    // Compute Cholesky factorization

    for (i=0;i<n;i++)
    {
        for (j=i;j<n;j++)
        {
            for (sum=A[i*n+j],k=i-1;k>=0;k--)
            {
                // printf("k=%i\n",k);
                sum -= A[i*n+k] * A[j*n+k];
                // printf("sum=%g\n", sum);
            }
            if (i == j)
            {
                if (sum <= 0.0) // A, with rounding errors, is not positive definite.
                {
                    printf("cholesky failed\n");
                    return;
                }
                // printf("i=%i\n", i);
                d[i]=sqrtf(sum);
                // printf("d[%i]=%g\n",i,d[i] );
            }
            else A[j*n+i]=sum/d[i];
        }
    }

    // print cholesky(A)
    if(DEBUG){
    printf("\nCholesky of A\n");
    for (i=0;i<n;i++)
        for (j=0;j<=i;j++) //print only lower triangular
        {
            if (i==j)
                printf("A[%i*%i+%i]=%g\n",i,n,j, d[i]);
            else
                printf("A[%i*%i+%i]=%g\n",i,n,j, A[i*n+j]);
        }
    }
    // Solve the systems
    for (i=0;i<n;i++)// Solve L*y = b, storing y in x.
    {
        for (sum=g[i],k=i-1;k>=0;k--) sum -= A[i*n+k] * h[k];
        h[i]=sum/d[i];
    }

    for (i=n-1;i>=0;i--) //Solve L^T*x = y.
    {
        for (sum=h[i],k=i+1;k<n;k++) sum -= A[k*n+i] * h[k];
        h[i]=sum/d[i];
    }

    // print the solution of the system, h
    if(DEBUG){
    for (i=0;i<n;i++)
        printf("h[%i]=%g\n",i,h[i] );
    }

    // check the residuals
    // now trial_y is used as a buffer for computing the predicted_residual
    // trial_y=J*h+y
    for (i=0;i<m;i++)
    {
        for (trial_y[i]=0.0,j=0;j<n;j++)
            trial_y[i] += J[i*n+j] * h[j];
        trial_y[i] += y[i];
    }

    for (predicted_residual=0.0,i=0;i<m;i++)
        predicted_residual += trial_y[i] * trial_y[i];

    for (j=0;j<n;j++)
        trial_x[j] = x[j] + h[j];

    (*func)(trial_x,trial_y,n,m);

    for (trial_residual=0.0,j=0;j<m;j++)
        trial_residual += trial_y[j] * trial_y[j];

    // step quality = residual change / predicted residual change
    rho = (trial_residual - residual) / (predicted_residual - residual);
    
    // print rho
    if(DEBUG){printf("rho=%g\n",rho);}

    if (rho > MIN_STEP_QUALITY)
    {
        for (j=0;j<n;j++)
            x[j] += h[j];
            
        for (i=0;i<m;i++)
            y[i]=trial_y[i];
        
        residual=trial_residual;
        lambda *= 0.1;
    }
    else lambda *= 10.0;

    // Compute the infinity norm of g_upd=J^T*y
    norm_inf=0.0;
    for (j=0;j<n;j++)
    {
        for (g_upd=0.0,i=0;i<m;i++)
            g_upd += J[i*n+j] * y[i];

        if(DEBUG){printf("g_upd=%g\n",g_upd);}

        if (g_upd < 0) g_upd = -g_upd; // take the abs.
        if (g_upd > norm_inf)
            norm_inf = g_upd;
    }
    if (norm_inf < TOL_GRAD) converged=1;
    //else if //TODO (michele)

//=====================
    if (VERBOSE || DEBUG){
        printf("\n==== iter %3i ====\n",iter_counter);
        printf("norm_inf=%g\n",norm_inf);
        printf("lambda=%g\n",lambda);
        for (i=0;i<n;i++)
            printf("h[%i]=%g\n",i,h[i] );
    }
    } //while loop
    *maxiter=iter_counter;
} // levenberg_marquardt

// Pointers to the chosen device functions
__device__ func_t p_func = FUNC;
__device__ func_t p_grad = GRAD;

int main(int argc, char const *argv[])
{
    const int n=4, m=11;
    int MAXITER=100;
    int *p_maxiter = &MAXITER;
    REAL x0[]={2.5, 3.9, 4.15, 3.9};
    int fixed_x[]={0,0,0,0};
    int n_fixed=4;
    int i,j;

    // Results
    REAL x[n];
    REAL y[m];
    REAL res;
    int iter=0;

    cudaError_t err;
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    

    // Define functions pointer
    func_t myfunc;
    func_t mygrad;

    // Load into the above defined pointers the device functions to be used
    HANDLE_ERR( cudaMemcpyFromSymbol( &myfunc, p_func, sizeof( func_t ) ));
    HANDLE_ERR( cudaMemcpyFromSymbol( &mygrad, p_grad, sizeof( func_t ) ));


    size_t sizeN = n*sizeof(REAL);
    size_t sizeM = m*sizeof(REAL);
    size_t sizeNN = n*n*sizeof(REAL);
    size_t sizeMN = m*n*sizeof(REAL);
    size_t isizeN= n*sizeof(int);

    // Pointers to data which will be allocated to the device
    REAL  *d_y      =0,
          *d_J      =0,
          *d_JtJ    =0,
          *d_A      =0,
          *d_g      =0,
          *d_diag   =0,
          *d_h      =0,
          *d_trial_x=0,
          *d_trial_y=0;

    REAL *d_x0=0;
    
    int *d_fixed_x=0,
        *d_maxiter=0;

    // Allocate vectors and matrices
    
    HANDLE_ERR( cudaMalloc((void**)&d_x0,sizeN) );
    HANDLE_ERR( cudaMalloc((void**)&d_fixed_x,sizeN) );
    HANDLE_ERR( cudaMemcpy(d_x0,x0,sizeN,cudaMemcpyHostToDevice) );
    HANDLE_ERR( cudaMemcpy(d_fixed_x,fixed_x,isizeN,cudaMemcpyHostToDevice) );
    
    HANDLE_ERR( cudaMalloc((void**)&d_y,sizeM) );
    HANDLE_ERR( cudaMalloc((void**)&d_trial_y,sizeM) );
    
    HANDLE_ERR( cudaMalloc((void**)&d_J, sizeMN) );

    HANDLE_ERR( cudaMalloc((void**)&d_JtJ, sizeNN) );

    HANDLE_ERR( cudaMalloc((void**)&d_A, sizeNN) );

    HANDLE_ERR( cudaMalloc((void**)&d_g,sizeN) );

    HANDLE_ERR( cudaMalloc((void**)&d_diag,sizeN) );

    HANDLE_ERR( cudaMalloc((void**)&d_h,sizeN) );

    HANDLE_ERR( cudaMalloc((void**)&d_trial_x,sizeN) );

    HANDLE_ERR( cudaMalloc((void**)&d_maxiter,sizeof(int)) );
    HANDLE_ERR( cudaMemcpy(d_maxiter,p_maxiter,sizeof(int),cudaMemcpyHostToDevice) );

    cudaEventRecord(start);
    levenberg_marquardt<<< 1,1>>>(myfunc,mygrad,
                                    d_x0, n, m, d_fixed_x, n_fixed,
                                    d_y, d_J, d_JtJ, d_A, d_g, d_diag,
                                    d_h, d_trial_x, d_trial_y, d_maxiter);

    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaDeviceSynchronize();
    err = cudaGetLastError();
    if (err != cudaSuccess) printf("error: %s\n",cudaGetErrorString(err));

    // Copy back the results
    HANDLE_ERR( cudaMemcpy(x,d_x0,sizeN,cudaMemcpyDeviceToHost) );
    HANDLE_ERR( cudaMemcpy(y,d_y,sizeM,cudaMemcpyDeviceToHost) );
    HANDLE_ERR( cudaMemcpy(&iter,d_maxiter,sizeof(int),cudaMemcpyDeviceToHost) );


    // Compute value of function at minimum
    for (res=0.0,j=0;j<m;j++)
        res += y[j];

    // Print results
    printf("\n==== RESULTS ====\n");
    printf("Levenberg Marquardt algorithm:\n");
    printf(" * Starting point: \n\tx0 = [ ");
    for (i=0;i<n;i++) printf(" %g ", x0[i]);
    printf(" ]\n");
    printf(" * Minimum: \n\tx  = [ ");
    for (i=0;i<n;i++) printf(" %g ", x[i]);
    printf(" ]\n");
    printf(" * Value of Function at Minimum: \n\t%g\n",res);
    printf(" * Iterations: \n\t%i\n",iter);

    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    printf("Kernel execution time: %g ms\n",milliseconds);

    //free memory
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    cudaFree(d_x0);
    cudaFree(d_fixed_x);
    cudaFree(d_y);
    cudaFree(d_trial_x);
    cudaFree(d_trial_y);
    cudaFree(d_J);
    cudaFree(d_JtJ);
    cudaFree(d_A);
    cudaFree(d_g);
    cudaFree(d_diag);
    cudaFree(d_h);
    cudaFree(d_maxiter);
    cudaDeviceReset();
    return err;
}
