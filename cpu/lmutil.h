#include <math.h>
#include <stdio.h>
#include "nrutil.h"
#define SWAP(a,b) {temp=(a);(a)=(b);(b)=temp;}

void covsrt(float **covar, const int ma, const int ia[], int mfit);
void gaussj(float **a, int n, float **b, int m);
void mrqcof(const float x[], const float y[], const float sig[], const int ndata, float a[],
        const int ia[], const int ma, float **alpha, float beta[], float *chisq,
     void (*funcs)(const float, float [], float *, float [], int));
