/*
 * lm.h
 *
 *  Created on: 10/dic/2014
 *      Author: michele
 */

#ifndef LM_H_
#define LM_H_

#define MAXITER 500

extern const int ndata;
extern const int ma;
extern const float x[],y[],sig[], init_a[];
extern const int ia[];
extern char dataset[];

void f1(const float x, float a[], float *yfit, float dyda[], const int ma);

#endif /* LM_H_ */
