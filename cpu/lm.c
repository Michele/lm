#include "lmutil.h"
#include "lm.h"
#include <stdio.h>
#include <stdlib.h>

void mrqmin(const float x[], const float y[], const float sig[], int ndata,
        float a[], const int ia[],
        const int ma, float **covar, float **alpha, float *chisq,
	void (*funcs)(float, float [], float *, float [], int),
	float *alamda)
/*
Levenberg-Marquardt method, attempting to reduce the value χ2 of a fit between a set of data
points x[1..ndata], y[1..ndata] with individual standard deviations sig[1..ndata],
and a nonlinear function dependent on ma coefficients a[1..ma]. The input array ia[1..ma]
indicates by nonzero entries those components of a that should be fitted for, and by zero
entries those components that should be held fixed at their input values. The program returns
current best-fit values for the parameters a[1..ma], and χ2 = chisq.
The arrays covar[1..ma][1..ma], alpha[1..ma][1..ma] are used as working space during most
iterations. Supply a routine funcs(x,a,yfit,dyda,ma) that evaluates the fitting function
yfit, and its derivatives dyda[1..ma] with respect to the fitting parameters a at x. On
the first call provide an initial guess for the parameters a, and set alamda<0 for initialization
(which then sets alamda=.001). If a step succeeds chisq becomes smaller and alamda decreases
by a factor of 10. If a step fails alamda grows by a factor of 10. You must call this
routine repeatedly until convergence is achieved. Then, make one final call with alamda=0, so
that covar[1..ma][1..ma] returns the covariance matrix, and alpha the curvature matrix.
(Parameters held fixed will return zero covariances.)*/
{
	int j,k,l;
	static int mfit;
	static float ochisq,*atry,*beta,*da,**oneda;
	if (*alamda < 0.0) //Initialization.
	{ 
		atry=vector(1,ma);
		beta=vector(1,ma);
		da=vector(1,ma);
		for (mfit=0,j=1;j<=ma;j++)
			if (ia[j]) mfit++;
		oneda=matrix(1,mfit,1,1);
		*alamda=0.001;
//		*alamda=100000;
		mrqcof(x,y,sig,ndata,a,ia,ma,alpha,beta,chisq,funcs);
		ochisq=(*chisq);
		for (j=1;j<=ma;j++)
			atry[j]=a[j];
	}


	for (j=1;j<=mfit;j++) // Alter linearized fitting matrix, by augmenting diagonal elements.
	{ 
		for	(k=1;k<=mfit;k++)
			covar[j][k]=alpha[j][k]; 
		covar[j][j]=alpha[j][j]*(1.0+(*alamda));
		oneda[j][1]=beta[j];
	}
	//FIXME (michele) remove me
//    for (j=1;j<=mfit;j++)
//            printf("oneda[%i][1]=%g\n",j,oneda[j][1]);

    gaussj(covar,mfit,oneda,1); // Matrix solution.

//    //FIXME (michele) remove me
//    for (j=1;j<=mfit;j++)
//        for (k=1;k<=j;k++)
//            printf("covar[%i][%i]=%g\n",j,k,covar[j][k]);
//
//    for (j=1;j<=mfit;j++)
//	        printf("oneda[%i][1]=%g\n",j,oneda[j][1]);

	for (j=1;j<=mfit;j++)
		da[j]=oneda[j][1];
	if (*alamda == 0.0) // Once converged, evaluate covariance matrix.
	{ 
		covsrt(covar,ma,ia,mfit);
		covsrt(alpha,ma,ia,mfit); // Spread out alpha to its full size too.
		free_matrix(oneda,1,mfit,1,1);
		free_vector(da,1,ma);
		free_vector(beta,1,ma);
		free_vector(atry,1,ma);
		return;
	}
	for (j=0,l=1;l<=ma;l++) // Did the trial succeed?
		if (ia[l])
			atry[l]=a[l]+da[++j];
	mrqcof(x,y,sig,ndata,atry,ia,ma,covar,da,chisq,funcs);
//	printf("chisq=%g; ochisq=%g\n",*chisq,ochisq);
	if (*chisq < ochisq)  // Success, accept the new solution.
	{
//	    printf("accepted!\n");
		*alamda *= 0.1;
		ochisq=(*chisq);
		for (j=1;j<=mfit;j++)
		{
			for (k=1;k<=mfit;k++)
				alpha[j][k]=covar[j][k];
			beta[j]=da[j];
		}
		for (l=1;l<=ma;l++)
			a[l]=atry[l];
	}
	else //Failure, increase alamda and return.
	{ 
		*alamda *= 10.0;
		*chisq=ochisq;
	}
}

int main(int argc, char * argv[])
{
    int k,j;
    float *a=vector(1,ma);

    for (k=1;k<=ma;k++)
    {
        a[k]=init_a[k];
    }

    float **covar=matrix(1,ma,1,ma);
    float **alpha=matrix(1,ma,1,ma);

    int maxiter;
    maxiter = (argc-1) ? atoi(argv[1]) : MAXITER;

    float chisq=0, old_chisq;
    float alamda=-1;
    int i;
    for (i=1;i<=maxiter;++i)
    {
        float deltachisq;
        old_chisq=chisq;
        mrqmin(x,y,sig, ndata, a, ia, ma, covar, alpha, &chisq, f1, &alamda);
        if (old_chisq != chisq)
        {
            printf("\n===== #%3i =====\n",i);
            for (k=1;k<=ma;k++)
                printf("a[%i]=%.10f\n",k,a[k]);
            printf("lambda=%g\n",alamda);
            printf("chisq=%g\n",chisq);
            deltachisq=old_chisq-chisq;
            printf("old_chisq-chisq=%g\n",deltachisq);
            if ((deltachisq>0 && deltachisq<1e-9) || alamda>1e39)
               break;
        }
    }
    alamda=0;
    mrqmin(x,y,sig, ndata, a, ia, ma, covar, alpha, &chisq, f1, &alamda);

    printf("\n====== RESULTS ======\n");
    printf("Data %s\n",dataset);
    printf("%3i iterations\n",i);
    for (k=1;k<=ma;k++)
        printf("a[%i]=%g\n",k,a[k]);
    printf("chisq=%g\n",chisq);

    printf("\n Covariance matrix:\n");
    for (j=1;j<=ma;j++)
        for (k=1;k<=j;k++)
            printf("covar[%i][%i]=%g\n",j,k,covar[j][k]);
    free_vector(a,1,ma);
    free_matrix(covar,1,ma,1,ma);
    free_matrix(alpha,1,ma,1,ma);
    return 0;
}

