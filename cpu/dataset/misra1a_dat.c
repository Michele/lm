#include <math.h>


char dataset[]="Misra1a";
const int ndata=14;
const int ma=2;

/* 
Misra1a data: http://www.itl.nist.gov/div898/strd/nls/data/misra1a.shtml
*/

const float x[]={0,
        77.6,
       114.9,
       141.1,
       190.8,
       239.9,
       289.0,
       332.8,
       378.4,
       434.8,
       477.3,
       536.8,
       593.1,
       689.1,
       760.0
};

const float y[]={0,
        10.07,
        14.73,
        17.94,
        23.93,
        29.61,
        35.18,
        40.02,
        44.82,
        50.76,
        55.05,
        61.01,
        66.40,
        75.47,
        81.78
};

const float sig[]={0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0};
const int ia[]={0,1,1};

const float init_a[]={0,500.,0.0001};                        // Start 1
// const float init_a[]={0,250.,0.0005};                       // Start 2
// const float init_a[]={0,2.3894212918E+02,5.5015643181E-04}; // Start 3

void f1(const float x, float a[], float *yfit, float dyda[], int ma)
{
    *yfit=a[1]*(1-exp(-a[2]*x));
    dyda[1]=1-exp(-a[2]*x);
    dyda[2]=a[1]*x*exp(-a[2]*x);
//    printf("f1 called: x=%6.2f a[1]=%.2f a[2]=%.2f yfit=%.2f\n",x,a[1],a[2],*yfit);
}
