#include <math.h>

char dataset[]="BoxBOD";

const int ndata=6;
const int ma=2;

/*
 BoxBOD data: http://www.itl.nist.gov/div898/strd/nls/data/boxbod.shtml
*/

const float x[]={0,
        1,
        2,
        3,
        5,
        7,
        10
};

const float y[]={0,
        109,
        149,
        149,
        191,
        213,
        224
};

const float sig[]={0,1.0,1.0,1.0,1.0,1.0,1.0};
const int ia[]={0,1,1};

//const float init_a[]={0,1.0,1.0};                            // Start 1
 const float init_a[]={0,100.,0.75};                         // Start 2
// const float init_a[]={0,2.1380940889E+02,5.4723748542E-01}; // Start 3

void f1(float x, float a[], float *yfit, float dyda[], int ma)
{
    *yfit=a[1]*(1-exp(-a[2]*x));
    dyda[1]=1-exp(-a[2]*x);
    dyda[2]=a[1]*x*exp(-a[2]*x);
//    printf("f1 called: x=%6.2f a[1]=%.2f a[2]=%.2f yfit=%.2f\n",x,a[1],a[2],*yfit);
}
