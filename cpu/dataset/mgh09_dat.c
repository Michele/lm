#include <math.h>

char dataset[]="MGH09";

const int ndata=11;
const int ma=4;

/*
 MGH09 data: http://www.itl.nist.gov/div898/strd/nls/data/mgh09.shtml
*/
const float x[]={0,
    4.000000E+00,
    2.000000E+00,
    1.000000E+00,
    5.000000E-01,
    2.500000E-01,
    1.670000E-01,
    1.250000E-01,
    1.000000E-01,
    8.330000E-02,
    7.140000E-02,
    6.250000E-02
};		

const float y[]={0,
    1.957000E-01,
    1.947000E-01,
    1.735000E-01,
    1.600000E-01,
    8.440000E-02,
    6.270000E-02,
    4.560000E-02,
    3.420000E-02,
    3.230000E-02,
    2.350000E-02,
    2.460000E-02
};

const float sig[]={0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0};

const int ia[]={0,1,1,1,1};

const float init_a[]={0,25, 39, 41.5, 39};             // Start 1
/* const float init_a[]={0,0.25, 0.39, 0.415, 0.39};      // Start 2
const float init_a[]={0,1.9280693458E-01, 1.9128232873E-01,
                  1.2305650693E-01, 1.3606233068E-01};  // Start 3
*/
void f1(const float x, float a[], float *yfit, float dyda[], int ma)
{
    float den=x*x+a[3]*x+a[4];
    float fact=x*(x+a[2]);
    *yfit   = (a[1]*fact)/den;
    dyda[1] = fact/den;
    dyda[2] = a[1]*x/den;
    dyda[4] = -a[1]*fact/(den*den);
    dyda[3] = dyda[4]*x;
//    printf("f1 called: x=%6.2f a[1]=%.2f a[2]=%.2f yfit=%.2f\n",x,a[1],a[2],*yfit);
}

