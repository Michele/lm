## NVCC Options
NVCC        = nvcc
NVCCFLAGS   = -g -G -O0 -arch=sm_35 -rdc=true
NVLIB 		= lcudadevrt
#NVCCINCLUDE = $(CULA_INC_PATH)
#NVCCLIBPATH = $(CULA_LIB_PATH_64)
#NVCCLIBS    = lcula_lapack_basic
# nvcc $(CUSRC) $(NVCCFLAGS) -I${NVCCINCLUDE} -L${NVCCLIBPATH} -${NVCCLIBS} -o $@

####### Files of the CUDA project
CUSRC  = log_likelihood_lm_gpu.cu
#CUOBJ  ={$CUSRC:.cu=.o}
CUPROG = lllg

####### Pattern rules
%.o :	%.cu
	$(NVCC) $(NVCCFLAGS) $(NVCCINCLUDE) -dc $< -o $@

#all: compile the program.
all: $(CUPROG)

$(CUPROG):
	$(NVCC) $(NVCCFLAGS) $(CUSRC) -$(NVLIB) -o $@

clean:
	rm ./*.o $(CUPROG)

.PHONY: all clean $(CUPROG)
