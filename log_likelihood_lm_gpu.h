/* Custom parameters */
#define REAL double

#define DEBUG 1
#define VERBOSE 1

// Requires the definition of err
#define HANDLE_ERR(err) {												\
	if (err != cudaSuccess)	{											\
		fprintf(stderr, "CUDA error detected: %s in %s at line %d\n",	\
				cudaGetErrorString( err ), __FILE__, __LINE__);			\
	} }
	